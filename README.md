# Styriabytes FileGenerator Library

This is a file generator library written in php.

## Contribution

Any type of feedback, pull request or issue is welcome.

We use the [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) to check for Coding Standard Issues.

### Check Coding Standard

```bash
vendor/bin/phpcs --standard=PSR1,PSR2,PSR12 --colors src
```

### Fix Errors Automatically

```bash
vendor/bin/phpcbf --standard=PSR1,PSR2,PSR12 src
```

### Run Tests

```bash
vendor/bin/phpunit
```

## License

The Styriabytes FileGenerator library is licensed under the MIT license.