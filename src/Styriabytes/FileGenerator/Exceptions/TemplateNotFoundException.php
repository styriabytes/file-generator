<?php

namespace Styriabytes\FileGenerator\Exceptions;

use Throwable;

/**
 * Class TemplateNotFoundException
 * @package Styriabytes\FileGenerator\Exceptions
 */
class TemplateNotFoundException extends \RuntimeException
{
    public function __construct($file = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct('Template file ' . $file . ' not found', $code, $previous);
    }
}
