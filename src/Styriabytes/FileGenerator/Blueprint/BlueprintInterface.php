<?php

namespace Styriabytes\FileGenerator\Blueprint;

/**
 * Interface BlueprintInterface
 *
 * @package Styriabytes\FileGenerator\Blueprint
 */
interface BlueprintInterface
{
    /**
     * Create the output value
     *
     * @return void
     */
    public function create(): void;

    /**
     * Return the output value
     *
     * @return null|string
     */
    public function getOutput(): ?string;

    /**
     * @return string
     */
    public function getOutputFile(): string;
}
