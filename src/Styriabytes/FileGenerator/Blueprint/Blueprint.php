<?php

namespace Styriabytes\FileGenerator\Blueprint;

use LogicException;
use Styriabytes\FileGenerator\Exceptions\TemplateNotFoundException;
use Styriabytes\FileGenerator\Support\TemplateTools;

/**
 * Class Blueprint
 *
 * @package Styriabytes\FileGenerator\Blueprint
 */
class Blueprint implements BlueprintInterface
{
    /** @var string */
    protected $templateFile;

    /** @var array */
    protected $validationRules = [];

    /** @var array */
    protected $data = [];

    /** @var null|string */
    private $output;

    /** @var null|string */
    private $outputFile;

    /**
     * Blueprint constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param string $file
     *
     * @return void
     */
    public function setTemplateFile(string $file): void
    {
        $this->templateFile = $file;
    }

    /**
     * @param string $file
     *
     * @return void
     */
    public function setOutputFile(string $file): void
    {
        $this->outputFile = $file;
    }

    /**
     * @return string
     */
    public function getOutputFile(): string
    {
        return $this->outputFile;
    }

    /**
     * @param array $rules
     *
     * @return void
     */
    public function setValidationRules(array $rules): void
    {
        $this->validationRules = $rules;
    }

    /**
     * @return bool
     */
    protected function validateTemplateData(): bool
    {
        foreach ($this->validationRules as $key => $rule) {
            if (!array_key_exists($key, $this->data)) {
                throw new LogicException("{$key} not found in template data.");
            }
        }
        return true;
    }

    /**
     * @throws \LogicException
     *
     * @return void
     */
    protected function validateTemplateFile(): void
    {
        if (!file_exists($this->templateFile)) {
            throw new LogicException("File '{$this->templateFile}' not found.");
        }
    }

    /**
     * @param array $data
     *
     * @return void
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * Create the output value
     *
     * @return void
     */
    public function create(): void
    {
        $this->validateTemplateFile();
        $this->validateTemplateData();

        $output = $this->getTemplateOutput($this->templateFile, $this->data);

        if (!$output) {
            // TODO(ssandriesser): error handling if output is false
        }

        $this->output = $output;
    }

    /**
     * Return the output value
     *
     * @return null|string
     */
    public function getOutput(): ?string
    {
        return $this->output;
    }

    /**
     * @param string $file
     * @param array  $data
     *
     * @return false|string
     */
    private function getTemplateOutput(string $file, array $data = [])
    {
        if (!TemplateTools::templateExists($file)) {
            throw new TemplateNotFoundException($file);
        }
        return TemplateTools::getTemplateContent($file, $data);
    }
}
