<?php

namespace Styriabytes\FileGenerator\Support;

/**
 * Class TemplateTools
 * @package Styriabytes\FileGenerator\Support
 */
class TemplateTools
{
    /**
     * Checks if the template file exists
     *
     * @param string $file
     *
     * @return bool
     */
    public static function templateExists(string $file): bool
    {
        return file_exists($file);
    }

    /**
     * Includes a template file with an isolated scope
     *
     * @param string $file_
     * @param array  $data_
     *
     * @return void
     */
    public static function includeTemplate(string $file_, array $data_ = []): void
    {
        extract($data_, EXTR_OVERWRITE);
        include $file_;
    }

    /**
     * Returns the rendered template content
     *
     * @param string $file
     * @param array  $data
     *
     * @return false|string
     */
    public static function getTemplateContent(string $file, array $data = [])
    {
        ob_start();
        static::includeTemplate($file, $data);
        return ob_get_clean();
    }

    /**
     * Prefix every line of the content
     *
     * @param string $content
     * @param string $prefix
     * @param bool   $prefixEmpty
     *
     * @return string
     */
    public static function prefixLines(string $content, string $prefix = '', bool $prefixEmpty = false): string
    {
        if ($prefix === '') {
            return $content;
        }

        $prefixedLines = [];
        foreach (explode(PHP_EOL, $content) as $line) {
            if (!$prefixEmpty && $line === '') {
                $prefixedLines[] = $line;
                continue;
            }
            $prefixedLines[] = $prefix . $line;
        }
        return implode(PHP_EOL, $prefixedLines);
    }
}
