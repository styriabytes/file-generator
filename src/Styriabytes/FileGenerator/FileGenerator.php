<?php

namespace Styriabytes\FileGenerator;

use Styriabytes\FileGenerator\Blueprint\BlueprintInterface;

/**
 * Class FileGenerator
 *
 * @package Styriabytes\FileGenerator
 */
class FileGenerator
{
    /** @var \Styriabytes\FileGenerator\Blueprint\BlueprintInterface[] */
    protected $blueprints = [];

    /**
     * FileGenerator constructor.
     */
    public function __construct()
    {
    }

    /**
     * Add a Blueprint to the FileGenerator
     *
     * @param \Styriabytes\FileGenerator\Blueprint\BlueprintInterface $blueprint
     *
     * @return void
     */
    public function addBlueprint(BlueprintInterface $blueprint): void
    {
        $this->blueprints[] = $blueprint;
    }

    /**
     * @return void
     */
    public function generate(): void
    {
        foreach ($this->blueprints as $blueprint) {
            $blueprint->create();
            $outputPath = dirname($blueprint->getOutputFile());
            if (!file_exists($outputPath) && !mkdir($outputPath, 0755, true) && !is_dir($outputPath)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $outputPath));
            }
            file_put_contents($blueprint->getOutputFile(), $blueprint->getOutput());
        }
    }
}
