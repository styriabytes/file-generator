<?php

namespace Styriabytes\FileGenerator\Parser;

/**
 * Class MysqlSchemaParser
 *
 * @package Styriabytes\FileGenerator\Parser
 */
class MysqlSchemaParser implements ParserInterface
{
    /**
     * @var array
     */
    private $data = array();

    /**
     * @var \PDO
     */
    protected $pdo;

    /**
     * @var string
     */
    protected $tableName;

    /**
     * @var string
     */
    protected $type = 'php';

    /**
     * MysqlSchemaParser constructor.
     */
    public function __construct()
    {
    }

    /**
     * Parse data
     *
     * @return void
     */
    public function parse()
    {
        $tableDescription = $this->getTableDescription($this->tableName);

        $this->data = [
            'tablename' => $this->tableName,
            'params' => $this->convertToEntityData($tableDescription),
        ];
    }

    /**
     * Return the parsed data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param \PDO $pdo
     */
    public function setPdo(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param string $tableName
     */
    public function setTable($tableName)
    {
        $this->tableName = $tableName;
    }


    public function setType($type)
    {
    }

    /**
     * @param string $tableName
     *
     * @return array
     */
    protected function getTableDescription($tableName)
    {
        $sql = "DESCRIBE `{$tableName}`";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param array $description
     *
     * @return array
     */
    protected function convertToEntityData(array $description)
    {
        $entityData = [];
        foreach ($description as $value) {
            $entityData[] = [
                'field' => $value['Field'],
                'type' => $this->convertType($value['Type'], $value['Field']),
                'nullable' => $value['Null'] === 'YES',
                'primary' => $value['Key'] === 'PRI',
            ];
        }
        return $entityData;
    }

    /**
     * @param string $type
     * @param string $fieldName
     *
     * @return string
     */
    protected function convertType($type, $fieldName)
    {
        if ($fieldName === 'id' || $this->contains($fieldName, ['_ID', '_Nr_']) || $this->endsWith($fieldName, ['Nr', '_id'])) {
            if ($this->type === 'php') {
                return 'int';
            }
            return 'id';
        }

        if ($this->isBool($type)) {
            return 'boolean';
        }

        if ($this->isInt($type)) {
            return 'int';
        }

        if ($this->isFloat($type)) {
            return 'float';
        }

        if ($this->isDatetime($type)) {
            if ($this->type === 'php') {
                return 'DateTime';
            }
            return 'string';
        }

        if ($this->isString($type)) {
            return 'string';
        }

        if ($this->isEnum($type)) {
            return 'string';
        }
        return $type;
    }

    protected function endsWith($haystack, $needles)
    {
        foreach ((array) $needles as $needle) {
            if (substr($haystack, -strlen($needle)) === (string) $needle) {
                return true;
            }
        }
        return false;
    }

    protected function contains($haystack, $needles)
    {
        foreach ((array) $needles as $needle) {
            if ($needle !== '' && mb_strpos($haystack, $needle) !== false) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $val
     *
     * @return bool
     */
    protected function isInt($val)
    {
        preg_match('/^(medium|tiny|small|big)?int(eger)?(\([\d]+\))?(\s(unsigned))?$/mi', $val, $matches, PREG_OFFSET_CAPTURE, 0);
        return count($matches) > 0;
    }

    /**
     * @param string $val
     *
     * @return bool
     */
    protected function isFloat($val)
    {
        preg_match('/^(dec(imal)?|float|fixed|real|double)(\sprecision)?(\([\d\,\s]+\))?(\s(unsigned))?/mi', $val, $matches, PREG_OFFSET_CAPTURE, 0);
        return count($matches) > 0;
    }

    /**
     * @param string $val
     *
     * @return bool
     */
    protected function isDatetime($val)
    {
        preg_match('/^(timestamp|date|time)$/im', $val, $matches, PREG_OFFSET_CAPTURE, 0);
        return count($matches) > 0;
    }

    /**
     * @param string $val
     *
     * @return bool
     */
    protected function isString($val)
    {
        preg_match('/^(enum\(.*\)|(long)?text|(var)?char(\([\d]+\))?|set\(.*\))$/im', $val, $matches, PREG_OFFSET_CAPTURE, 0);
        return count($matches) > 0;
    }

    /**
     * @param string $val
     *
     * @return bool
     */
    protected function isBool($val)
    {
        preg_match('/^(tiny)?int(eger)?(\([1]\))?(\s(unsigned))?$/mi', $val, $matches, PREG_OFFSET_CAPTURE, 0);
        return count($matches) > 0;
    }

    /**
     * @param string $val
     *
     * @return bool
     */
    protected function isEnum($val)
    {
        preg_match('/^(enum).*$/mi', $val, $matches, PREG_OFFSET_CAPTURE, 0);
        return count($matches) > 0;
    }
}
