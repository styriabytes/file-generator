<?php

namespace Styriabytes\FileGenerator\Parser;

/**
 * Interface ParserInterface
 *
 * @package Styriabytes\FileGenerator\Parser
 */
interface ParserInterface
{
    /**
     * Parse data
     *
     * @return void
     */
    public function parse(): void;

    /**
     * Return the parsed data
     *
     * @return array
     */
    public function getData(): array;
}
