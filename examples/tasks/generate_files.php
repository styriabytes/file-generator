<?php
require_once __DIR__ . '/../../vendor/autoload.php';

// Create a Task entity
$blueprint = new \Styriabytes\FileGenerator\Blueprint\Blueprint();
$blueprint->setTemplateFile(__DIR__.str_replace('/', DIRECTORY_SEPARATOR, '/templates/entity.template.php'));
$blueprint->setOutputFile(__DIR__.str_replace('/', DIRECTORY_SEPARATOR, '/output/Styriabytes/App/Task.php'));
$blueprint->setData([
    'namespace' => 'Styriabytes\\App',
    'classname' => 'Task',
    'properties' => [
        'title' => [
            'type' => 'string',
            'default' => "''",
            'nullable' => false,
        ],
        'description' => [
            'type' => 'string',
            'default' => 'null',
            'nullable' => true,
        ],
        'checked' => [
            'type' => 'bool',
            'default' => 'false',
            'nullable' => false,
        ],
    ],
]);

$fileGenerator = new \Styriabytes\FileGenerator\FileGenerator();
$fileGenerator->addBlueprint($blueprint);

$fileGenerator->generate();