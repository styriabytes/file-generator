<?php

namespace Styriabytes\App;

/**
 * Class Task
 * @package Styriabytes\App
 *
 * @note autogenerated: 2020-04-20 00:31:22
 */
class Task
{
    /** @var string $title */
    protected $title = '';

    /** @var null|string $description */
    protected $description;

    /** @var bool $checked */
    protected $checked = false;

    /**
     * Task constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param string $title
     *
     * @return void
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param null|string $description
     *
     * @return void
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param bool $checked
     *
     * @return void
     */
    public function setChecked(bool $checked): void
    {
        $this->checked = $checked;
    }

    /**
     * @return bool
     */
    public function isChecked(): bool
    {
        return $this->checked;
    }
}